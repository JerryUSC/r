Star = read.table("SA11984_fpkm.txt")
fpkm_star=Star$V2
fpkm_tophat=Star$V3
fpkm = data.frame(star=Star$V2, tophat=Star$V3)
fpkm_log = na.omit(data.frame(star=log(Star$V2), tophat=log(Star$V3)))
fpkm_l <- fpkm_log[!is.infinite(rowSums(fpkm_log)),]

r = cor(fpkm$star, fpkm$tophat)
r^2
g <- ggplot(fpkm, aes(star, tophat))+
  geom_point() +
  geom_smooth(method="lm", se=F, siz  = 100)+
  labs(subtitle="mpg: city vs highway mileage", 
       y="tophat2", 
       x="STAR", 
       title="FPKM of genes using tophat2 and STAR aligner, R^2 = 99%", 
       caption="Source: midwest")
g

r = cor(fpkm_l$star, fpkm_l$tophat)
r^2
g <- ggplot(fpkm_l, aes(star, tophat))+
  geom_point() +
  geom_smooth(method="lm", se=F, siz  = 100)+
  labs(subtitle="mpg: city vs highway mileage", 
       y="tophat2", 
       x="STAR", 
       title="lg(FPKM) of genes using tophat2 and STAR aligner, R^2 = 99.6%", 
       caption="Source: midwest")
g