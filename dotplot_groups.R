library(ggplot2)
library(scales)
theme_set(theme_classic())

file <- read.table("Tumor_contamination.txt")

Netera <- data.frame(Sample=file$V1,Contamin=file$V2*100)
Netera <- Netera[seq(dim(Netera)[1],1),]

ggplot(Netera, aes(x=Sample, y=Contamin)) +
  geom_point() +
  geom_segment(aes(x=Sample, 
                   xend=Sample, 
                   y=min(Contamin), 
                   yend=max(Contamin)), 
               linetype="dashed", 
               size=0.1) +   # Draw dashed lines
  labs(title="GATK contamintionCalculate",y="Contamination %")+
  coord_flip()


#### Lab QC  #####
labinput <- read.table("LabLookup.txt")
labqc <- data.frame(Sample=labinput$V1, amount<-labinput$V4, dv500<-labinput$V8)
#### The DV500 ##

ggplot(labqc, aes(x=Sample, y=amount)) +
  geom_point() +
  geom_segment(aes(x=Sample, 
                   xend=Sample, 
                   y=min(amount), 
                   yend=max(amount)), 
               linetype="dashed", 
               size=0.1) +   # Draw dashed lines
  geom_hline(aes(yintercept=100),color="red")+
  #labs(title="GATK contamintionCalculate",y="Contamination %")+
  theme(axis.text=element_text(size=6))+
  coord_flip()



ggplot(labqc, aes(x=Sample, y=dv500)) +
  geom_point() +
  geom_segment(aes(x=Sample, 
                   xend=Sample, 
                   y=min(dv500), 
                   yend=max(dv500)), 
               linetype="dashed", 
               size=0.1) +   # Draw dashed lines
  geom_hline(aes(yintercept=50),color="red")+
  #labs(title="GATK contamintionCalculate",y="Contamination %")+
  theme(axis.text=element_text(size=6))+
  coord_flip()

which(labqc$amount<100)
which(labqc$dv500<50)
