###############
Primer3 design
##############

qPCR: add 150bp from intron region and then select the best result
	PRIMER_MIN_SIZE=17
	PRIMER_OPT_SIZE=22
	PRIMER_MAX_SIZE=30
	PRIMER_MIN_TM=59.0
	PRIMER_OPT_TM=62.0
	PRIMER_MAX_TM=65.0
	PRIMER_SALT_MONOVALENT=50.0
	PRIMER_SALT_DIVALENT=3
	PRIMER_DNTP_CONC=0.8
	PRIMER_DNA_CONC=200
	PRIMER_MIN_GC=35.0
	PRIMER_MAX_GC=65.0
	PRIMER_OPT_GC_PERCENT=50
	PRIMER_GC_CLAMP=0
	PRIMER_PRODUCT_SIZE_RANGE=75-150
	PRIMER_PRODUCT_OPT_SIZE=100
	PRIMER_NUM_RETURN=10
	PRIMER_MIN_THREE_PRIME_DISTANCE=5
	SEQUENCE_TARGET=150,160
	PRIMER_THERMODYNAMIC_PARAMETERS_PATH=/home/clee/Primer/primer3-2.3.6/test/primer3_config/
	=

Sanger: add 250bp from intron region and then choose at least 50bp away from exon region. If the length >700bp, select 2 sets of primers which overlap at least 100bp
	PRIMER_MIN_SIZE=17
	PRIMER_OPT_SIZE=22
	PRIMER_MAX_SIZE=30
	PRIMER_MIN_TM=59.0
	PRIMER_OPT_TM=62.0
	PRIMER_MAX_TM=65.0
	PRIMER_SALT_MONOVALENT=50.0
	PRIMER_SALT_DIVALENT=3
	PRIMER_DNTP_CONC=0.8
	PRIMER_DNA_CONC=200
	PRIMER_MIN_GC=35.0
	PRIMER_MAX_GC=65.0
	PRIMER_OPT_GC_PERCENT=50
	PRIMER_GC_CLAMP=0
	PRIMER_PRODUCT_SIZE_RANGE=200-750
	PRIMER_NUM_RETURN=10
	PRIMER_MIN_THREE_PRIME_DISTANCE=3
	SEQUENCE_EXCLUDED_REGION=200,178
	PRIMER_THERMODYNAMIC_PARAMETERS_PATH=/home/clee/Primer/primer3-2.3.6/test/primer3_config/
	=

Previous software:
	BatchPD2primers
	
To detect homozygous delection - Sanger
						  CNV  -  qPCR
Usually less than 10% of primers have bad performance

$BIOINFX/ngs/targets/Illumina_TruSight1_pad10_withGene_1405.bed   -- have the defined pos for each targets   (check refGene_exonsCDS.bed too)
PerTargetCoverage_Stats_1506316_Clinical-Exome.txt                -- The real target region

Step: 
    Use primer3 to design 10 sets of primers fora each, and then check in-silico pcr. Only use the region can produce single amplicon. Then filter out the primers have SNP inside.

######################
phenotype to gene
#####################
1. HPO database - http://compbio.charite.de/hudson/job/hpo.annotations.monthly/lastStableBuild/artifact/annotation/ALL_SOURCES_ALL_FREQUENCIES_diseases_to_genes_to_phenotypes.txt
2. ClinVar - ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh37/clinvar.vcf.gz 



########################################
Convert cnomen to chromosome position
########################################



###########
Lab Data
###########
DNA gel 
	/mnt/fvg01vol4/Gel Images/BioRad Agarose Gel Images/DNA extraction 2015
qPCR (1505734)
	RQ vs Sample.jpg
Sanger (ab1) open by Muration Surveyor
	Common\CLIA\Bioinformatics\data\2015 ABI runs
	Folder name: 
		Seq-Comb :Combined plate from old plate (old primers)
		
	Naming Rule: 
		accession-gene-exon-Condition-F/R-wellPosition-year-month-day.ab1
		condition: ex. 5D,10D,5DQ
		
Alan
	Common\CLIA\Wet Lab\SangerSeqTest.100815.az\OnGoing
MPLA (fsa)
	Common\CLIA\Bioinformatics\data\2015 ABI runs
	xps file inside the report/accession (ex:1507554)
	accession-gene-wellPosition.fsa
	mix is a control group we mix from several patients
	0_ means we don't load anythin in that wellPosition
	NTC_
	AMP_  study cases by Sarah
	
	
###### By Jimmy  ########
Accession oriented table - to tell what option they should create (NGS: TNXB,PKD1)
gene exon chr start end action #dup

Sanger  c.94C>T (p.Arg32Cys) in AJ549245 when Dystonia Dyskinesia NGS Panel ordered?
RP-PCR  :  create task for gene HD,HTT (CAG>40 times), C9ORF72 (5bp>23 times)
order test
	custom 5 discount
	custom 3 curator
	custom 2 FT status
	custom 8 Priority
	custom 6 Custom detail (gene list)
	Custom 4 added gene
checkbox5 - STAT -> ASAP

##########  AA conservation #################
100way from UCSC
Primate 12
Mammal 62


##########  Google search tools  ##############

Input: NM_012213.2:c.949-17C>T
Output: "MLYCD" ("949-17C>T" | "IVS4-17" | "IVS 4-17" | "949-17C->T" | "949-17C-->T" | "949-17C/T")

Input: NM_004248.2:c.264_271dup
Output: "PRLHR" ("264_271dup" | "264-271dup" | "Leu91Argfs*7" | "Leu91ArgfsX7" | "Leu91ArgfsTer7" | "Leu91Argfs" | "L91Rfs*7" | "L91RfsX7" | "L91Rfs")

##########  CNV Plus ################

######### Misalignment pipeline #########

######### Junction reads ############

######## Reference genome test and modification


###### Detection contamination percentage  ##################

##### Mitochondria deletion ######

##### Expansion hunter ######

#### a-tail ligation noise  #####