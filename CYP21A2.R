library(ggplot2)
library("gridExtra")
df <- data.frame(exon=c("1","2","3","4","5","6","7","8","9","10"),
                len=c(-0.010050336,-0.030459207,0.139761942,0.067658648,0.09531018,0.122217633,0.108854405,0.783901544,2.387844937,1.414638834),
                errors=c(0.037,0.036,0.035,0.033,0.032,0.033,0.031,0.04,0.045,0.058))
# 		    errors=c(1.16,1.15,1.1,1.05,1.03,1.04,1,1.28,1.44,1.84))
df$exon<- factor(df$exon, levels=c("1","2","3","4","5","6","7","8","9","10"))

p1 <-ggplot(data=df, aes(x=exon, y=len)) +
  geom_bar(stat="identity", fill="steelblue")+
  coord_cartesian(ylim=c(-3,3)) +
  geom_hline(aes(yintercept=1.0986), colour="#990000", linetype="dashed")+ 
  geom_hline(aes(yintercept=-1.0986), colour="#990000", linetype="dashed")+
  geom_errorbar(aes(ymin=len-errors, ymax=len+errors), width=.2,position=position_dodge(.9)) +
  labs(title="(a) population", x="Exon number",y="Misalignment index") 
  
e1=read.table("CYP21A2_Rinput.txt")

exondata <- data.frame(exon=e1$V1,mi=log(e1$V2))
exondata$exon <- as.factor(exondata$exon)
p1 <- ggplot(exondata , aes(x=exon,y=mi , fill=exon)) + 
  geom_violin(show.legend =TRUE)+
  scale_fill_brewer(palette="Set3")+
  scale_fill_discrete(guide=FALSE)+
  coord_cartesian(ylim=c(-3,3))+
  #geom_boxplot(width=0.1, fill="white")+
  geom_hline(aes(yintercept=1.0986), colour="#cc3300", size=1, linetype="dashed")+
  geom_hline(aes(yintercept=-1.0986), colour="#0066ff", size=1, linetype="dashed")+
  #geom_jitter(shape=16, position=position_jitter(0.2),colour=exon)  
  labs(title="(a) population", x="Exon number",y="Misalignment index")+
  theme_classic()#+
  #theme(text = element_text(size=20))


exon3 <- data.frame(exon=c("1","2","3","4","5","6","7","8","9","10"),
                len=c(-1.122089324,-1.06784063,-0.961311414,-0.55313395,0.11058876,0.048790164,-0.123175312,1.133098465,1.785567499,0.488561253))

exon3$exon<- factor(exon3$exon, levels=c("1","2","3","4","5","6","7","8","9","10"))


## 1701582


p2 <- ggplot(data=exon3, aes(x=exon, y=len, fill=exon)) +
  geom_bar(stat="identity")+
  scale_fill_brewer(palette="Set3")+
  coord_cartesian(ylim=c(-3,3)) +
  geom_hline(aes(yintercept=1.0986), colour="#990000", linetype="dashed")+
  geom_hline(aes(yintercept=-1.0986), colour="#990000", linetype="dashed")+ 
  labs(title="(c)", x="Exon number",y="Misalignment index") 
 
 
exon32 <- data.frame(exon=c("1","2","3","4","5","6","7","8","9","10"),
                len=c(-1.108662625,-1.966112856,-4.605170186,-1.347073648,-1.021651248,-0.994252273,-1.049822124,0.019802627,1.763017,0.425267735))

exon32$exon<- factor(exon32$exon, levels=c("1","2","3","4","5","6","7","8","9","10"))

## 1706437
p3 <-ggplot(data=exon32, aes(x=exon, y=len), fill=exon) +
  geom_bar(stat="identity")+
  scale_fill_brewer(palette="Set3")+
  coord_cartesian(ylim=c(-3,3)) +
  geom_hline(aes(yintercept=1.0986), colour="#990000", linetype="dashed")+
  geom_hline(aes(yintercept=-1.0986), colour="#990000", linetype="dashed")+ 
  labs(title="(d)", x="Exon number",y="Misalignment index") + 
  theme_classic()

exon1 <- data.frame(exon=c("1","2","3","4","5","6","7","8","9","10"),
                len=c(-1.021651248,-0.494296322,0.039220713,0.122217633,0.113328685,0.122217633,-0.15082289,0.494696242,1.568615918,0.470003629))

exon1$exon<- factor(exon1$exon, levels=c("1","2","3","4","5","6","7","8","9","10"))

## 1708217
p4 <-ggplot(data=exon1, aes(x=exon, y=len)) +
  geom_bar(stat="identity", fill="steelblue")+
  coord_cartesian(ylim=c(-3,3)) +
  geom_hline(aes(yintercept=1.0986), colour="#990000", linetype="dashed")+
  geom_hline(aes(yintercept=-1.0986), colour="#990000", linetype="dashed")+ 
  labs(title="(b)", x="Exon number",y="Misalignment index") 

dup <- data.frame(exon=c("1","2","3","4","5","6","7","8","9","10"),
                len=c(0.94,0.7,1.04,0.94,0.63,0.4,0.39,1.63,3.8,5.69))

dup$exon<- factor(dup$exon, levels=c("1","2","3","4","5","6","7","8","9","10"))


##  1714058
p5 <-ggplot(data=dup, aes(x=exon, y=len)) +
  geom_bar(stat="identity", fill="steelblue")+
  coord_cartesian(ylim=c(-3,3)) +
  geom_hline(aes(yintercept=1.0986), colour="#990000", linetype="dashed")+
  geom_hline(aes(yintercept=-1.0986), colour="#990000", linetype="dashed")+ 
  labs(title="(e)", x="Exon number",y="Misalignment index") 


del<- data.frame(exon=c("1","2","3","4","5","6","7","8","9","10"),
                len=c(0.27,0.47,1.45,0.36,-0.19,0.62,0.88,0.62,2.73,3.54))

del$exon<- factor(del$exon, levels=c("1","2","3","4","5","6","7","8","9","10"))


## 1713071
p6 <-ggplot(data=del, aes(x=exon, y=len)) +
  geom_bar(stat="identity", fill="steelblue")+
  coord_cartesian(ylim=c(-3,3)) +
  geom_hline(aes(yintercept=1.0986), colour="#990000", linetype="dashed")+
  geom_hline(aes(yintercept=-1.0986), colour="#990000", linetype="dashed")+ 
  labs(title="(f)", x="Exon number",y="Misalignment index") 


grid.arrange(p1, p4, p2, p3, p5, p6, nrow = 2)

